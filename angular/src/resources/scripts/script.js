'use strict';

document.addEventListener('DOMContentLoaded', () => {

    class Anim {

        constructor(cssClass, imageCount, timeoutMs) {
            this.cnt = 0;
            this.max = imageCount;
            this.cssClass = cssClass;
            this.timeoutMs = timeoutMs;
            this.wrapper = document.querySelector('.ovece-view');
        }

        switchImages() {
            this.cnt = (this.cnt + 1) % this.max;
            this.wrapper.querySelectorAll(`.${this.cssClass}`).forEach(elem => elem.classList.remove('active'));
            this.wrapper.querySelector(`.${this.cssClass}-0${this.cnt}`).classList.add('active');
        }

        run() {
            setTimeout(() => {
                this.switchImages();
                this.run();
            }, this.timeoutMs);
        }

    }


    new Anim('potok', 2, 2000).run();
    new Anim('psice', 3, 700).run();
    new Anim('grass-a', 4, 400).run();
    new Anim('grass-b', 4, 450).run();
    new Anim('tree', 5, 500).run();
    new Anim('leaves', 4, 500).run();

});
