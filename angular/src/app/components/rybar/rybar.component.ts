import { Component, Input, OnInit, ElementRef, ViewChild } from '@angular/core';

@Component({
    selector: 'app-rybar',
    templateUrl: './rybar.component.html',
    styleUrls: ['./rybar.component.scss']
})
export class RybarComponent implements OnInit {

    @Input() fishMerryEnabled: boolean = false;
    @Input() fishRowsEnabled: boolean = false;

    ngOnInit() {
        const lucky_radio = Math.floor(Math.random() * 2);
        if (lucky_radio === 0) {
            this.fishMerryEnabled = true;
        } else {
            this.fishRowsEnabled = true;
        }
    }

}
