import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-place',
    templateUrl: './place.component.html',
    styleUrls: ['./place.component.scss'],
    preserveWhitespaces: true
})
export class PlaceComponent implements OnInit {

    shownInfoType: string = '';
    presentItemCategories: any[] = [];
    managedPresentItem: any = null;
    showManagePresentPopup: boolean = false;

    loggedInClaimCode: string = '';
    loggedInClaimName: string = '';
    showLoginDialog: boolean = false;
    loginDialogCode: string = '';

    cookieName: string = 'loggedIn';
    mapUrl: string = 'https://en.mapy.cz/zakladni?x=14.4166167&y=50.8510623&z=17&source=addr&id=11445222';

    transportShowBusDetail: boolean = false;

    @Output() onCloseArea: EventEmitter<any> = new EventEmitter();

    @ViewChild('loginCodeInput') loginCodeInput?: ElementRef;

    constructor(private http: HttpClient) { }

    ngOnInit(): void {
    }

    ngAfterViewInit() {
        this.loadCookieLoggedIn();
        this.loadData();
    }

    loadData() {
        this.http.get(`/service/present/list?claimCode=${this.loggedInClaimCode}`, { responseType: 'json' }).subscribe(
            (res: any) => {
                console.log('presents success - res:', res);
                this.presentItemCategories = res;
                this.presentItemCategories.forEach(category => category.items.forEach((item: any) => item.categoryId = category.id));
                console.log('prepared: ', this.presentItemCategories);
            },
            (err: any) => {
                console.log('presents error:', err);
            }
        );
    }

    loadCookieLoggedIn() {
        const data = this.getCookieValue(this.cookieName);

        if (!data) {
            return;
        }

        const json = JSON.parse(decodeURIComponent(atob(data)));
        console.log('loaded from cookie:', json);

        this.loggedInClaimCode = json.code;
        this.loggedInClaimName = json.name;
    }

    closeArea() {
        this.onCloseArea.emit();
    }

    removePresent(category: any, item: any): void {
        if (!this.loggedInClaimCode) {
            alert('Musíte se přihlásit odpovídajícím kódem.');
            return;
        }
        this.http.delete(`/service/present?id=${item.id}&claimCode=${this.loggedInClaimCode}`, { responseType: 'text' }).subscribe({
            next: (res: any) => {
                console.log('present delete success - res:', res);

                category.items.splice(category.items.indexOf(item), 1);

            },
            error: (err: any) => {
                console.log('present delete error - err:', err);
                alert('Položka patří jinému uživateli.');
            },
        });
    }

    savePresent(item: any): void {
        item.errorMessage = '';

        const method = item.justCreated ? 'POST' : 'PUT';

        if (!item.justCreated && !this.loggedInClaimCode) {
            item.errorMessage = 'Je třeba vložit kód.';
            return;
        }

        if (!item.claimNameNew || !item.title) {
            item.errorMessage = 'Musíte vyplnit jméno i název pokrmu.';
            return;
        }

        const data = {
            'id': item.id || null,
            'claimCode': this.loggedInClaimCode || null,
            'claimName': item.claimNameNew,
            'categoryId': item.categoryId,
            'title': item.title || '',
            'description': item.description || '',
        };
        console.log(`request ${method} with body:`, data);
        this.http.request(method, '/service/present', { body: data, responseType: 'text' }).subscribe({
            next: (res: any) => {
                console.log('present submit success - res:', res);
                this.showManagePresentPopup = false;
                this.managedPresentItem = null;

                if (item.justCreated) {
                    this.setLoggedIn(res, item.claimNameNew);
                } else {
                    this.setLoggedIn(this.loggedInClaimCode, item.claimNameNew);
                }

                this.loadData();
            },
            error: (err: any) => {
                console.log('present submit error - err:', err);
            },
        });
    }

    addItem(category: any): void {
        category.items.unshift({
            claimNameNew: this.loggedInClaimName || '',
            categoryId: category.id,
            editing: true,
            editable: true,
            justCreated: true,
        });
    }

    setLoggedIn(code: string, name: string): void {
        this.loggedInClaimCode = code;
        this.loggedInClaimName = name;
        const json = { 'code': code, 'name': name };
        const data = btoa(encodeURIComponent(JSON.stringify(json)));
        this.setCookieValue(this.cookieName, data);
    }

    setCookieValue(cookieName: string, cookieValue: string) {
        const exdate = new Date();
        exdate.setDate(exdate.getDate() + 365);
        document.cookie = `${cookieName}=${cookieValue}; expires=${exdate.toUTCString()}; path=/`;
    }

    getCookieValue(cookieName: string) {
        const result = document.cookie.match('(^|;)\\s*' + cookieName + '\\s*=\\s*([^;]+)');
        return result ? result.pop() : '';
    }

    logIn() {
        this.http.get('/service/present/login?claimCode=' + this.loginDialogCode, { responseType: 'json' }).subscribe({
            next: (res: any) => {
                console.log('login success - res:', res);
                this.setLoggedIn(res.claimCode, res.claimName);
                this.showLoginDialog = false;
                this.loadData();
            },
            error: (err: any) => {
                console.log('login error - err:', err);
            },
        });
    }

    logOut() {
        this.setLoggedIn('', '');
        this.loadData();
    }

    cancelEditing(category: any, item: any): void {
        item.editing = false;
        if (item.justCreated) {
            category.items.splice(category.items.indexOf(item), 1);
        }
    }

    expandItem(item: any): void {
        if (!item.editable || item.editing) {
            return;
        }
        item.editing = true;
        item.claimNameNew = item.claimName;
    }

    showLoginDialogFn(): void {
        this.showLoginDialog = true;
        this.loginDialogCode = '';
        setTimeout(() => {
            this.loginCodeInput?.nativeElement.focus();
        }, 10);

    }

}
