import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';

enum AuthorType {
    HE = 'he',
    SHE = 'she',
}

interface Letter {
    bubbles: {
        date: string,
        author: AuthorType,
        textLines: string[],
        img: string,
    }[],
}

@Component({
    selector: 'app-love-letters',
    templateUrl: './love-letters.component.html',
    styleUrls: ['./love-letters.component.scss']
})
export class LoveLettersComponent implements OnInit {

    letters: Letter[] = [];

    constructor(private http: HttpClient) { }

    ngOnInit(): void {
    }

    ngAfterViewInit() {
        this.loadLetters();
    }

    loadLetters() {
        this.letters = [];
        const self = this;

        this.http.get(`/assets/dopisy.txt`, { responseType: 'text' }).subscribe({
            next: (res: string) => {
                console.debug('letters success - res:', res);
                let letter: Letter | null = null;
                res.split('\n').map(line => line.trim()).filter(line => line.length > 0).forEach((line: string) => {
                    if (line === '---') {
                        if (letter !== null) {
                            self.letters.push(letter);
                            letter = null;
                        }
                        return;
                    }
                    if (letter === null) {
                        letter = { bubbles: [] };
                    }
                    if (line.startsWith('Mvín:')) {
                        const date = line.substring(5);
                        letter.bubbles.push({
                            date: date,
                            author: AuthorType.HE,
                            textLines: [],
                            img: '',
                        });
                        return;
                    }
                    if (line.startsWith('Mšen:')) {
                        const date = line.substring(5);
                        letter.bubbles.push({
                            date: date,
                            author: AuthorType.SHE,
                            textLines: [],
                            img: '',
                        });
                        return;
                    }
                    if (line.startsWith('[img|')) {
                        letter.bubbles[letter.bubbles.length - 1].img = line.substring(5, line.length - 1);
                        return;
                    }
                    letter.bubbles[letter.bubbles.length - 1].textLines.push(line);
                });
                if (letter !== null) {
                    self.letters.push(letter);
                }
                console.debug('prepared: ', self.letters);
            },
            error: (err: any) => {
                console.debug('letters error:', err);
            }
        });
    }

}
