import { Component, OnInit, ElementRef, ViewChild, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StateService } from '../../service/state.service';

enum Direction {
    NEXT,
    PREV,
}

@Component({
    selector: 'app-photos',
    templateUrl: './photos.component.html',
    styleUrls: ['./photos.component.scss']
})
export class PhotosComponent implements OnInit {

    showRybar: boolean = false;
    photoUrls: string[] = [];
    currentIndex: number = -1;
    photoViewedPercentage: number = 0;
    photoUrl: string = '';
    showArrows: boolean = true;
    showCounter: boolean = true;
    showTools: boolean = true;

    password: string = '';
    unlocked: boolean = false;
    lockedCss: string = '';

    hideCounterTimeout: any = null;
    hideRybarTimeout: any = null;

    constructor(private http: HttpClient, private state: StateService) {
        this.state.swipeLeft.subscribe((event) => {
            this.showArrows = false;
            this.prev();
        });
        this.state.swipeRight.subscribe((event) => {
            this.showArrows = false;
            this.next();
        });
    }

    ngOnInit(): void {
    }

    ngAfterViewInit() {
        this.loadData();
    }

    loadData(): void {
        this.photoUrls = [];
        const self = this;

        this.http.get<string[]>(`/service/photo/static/manifest.json?mode=base`).subscribe({
            next: (res: string[]) => {
                console.debug('photos success - res:', res);

                this.logInFinished(true, () => {
                    self.unlocked = true;
                    self.photoUrls = res.map(name => `/service/photo/static/${name}?mode=base`);
                    self.next();
                });

            },
            error: (err: any) => {
                console.debug('photos error:', err);
            }
        });
    }

    logInFinished(success: boolean, callback: Function = () => {}): void {
        this.lockedCss = 'trans1';
        const self = this;
        setTimeout(() => {
            self.lockedCss = 'trans2';
            setTimeout(() => {
                if (success) {
                    self.lockedCss = '';
                    callback();
                    return;
                }
                self.lockedCss = 'trans3';
                setTimeout(() => {
                    self.lockedCss = '';
                    callback();
                }, 1000);
            }, 300);
        }, 1000);
    }

    loadImg(photoUrl: string, nextUrl: string): void {
        this.showRybar = true;

        const self = this;
        let loaded1 = false;
        let loaded2 = false;

        const onload = () => {
            if (!loaded1 || !loaded2) {
                return;
            }
            const timeout = 0;//Math.floor(Math.random() * 3) * 100;
            clearTimeout(self.hideRybarTimeout);
            self.hideRybarTimeout = setTimeout(() => {
                self.showRybar = false;
            }, timeout);
        };

        var img1 = new Image();
        var img2 = new Image();
        img1.onload = () => { loaded1 = true; self.photoUrl = photoUrl; onload(); };
        img2.onload = () => { loaded2 = true; onload(); };
        img1.src = photoUrl;
        img2.src = nextUrl;
    }

    percentageChanged(percentage: number): void {
        console.log('percentageChanged');
        if (this.photoUrls.length === 0) {
            return;
        }

        this.currentIndex = Math.ceil(this.photoUrls.length * (percentage / 100) - 1);
        console.log('currentIndex:', this.currentIndex);
        this.nextOrPrev(Direction.NEXT);
    }

    nextOrPrev(direction: Direction) {
        if (this.photoUrls.length === 0) {
            return;
        }

        this.currentIndex = this.getNextIndex(this.currentIndex, direction);
        const nextIndex = this.getNextIndex(this.currentIndex, direction);

        this.photoViewedPercentage = Math.ceil((this.currentIndex+1) / this.photoUrls.length * 100);

        const photoUrl = this.photoUrls[this.currentIndex];
        const nextUrl = this.photoUrls[nextIndex];

        this.loadImg(photoUrl, nextUrl);

        this.showCounter = true;
        if (this.currentIndex+1 === this.photoUrls.length || this.currentIndex === 0) {
            return;
        }
        clearTimeout(this.hideCounterTimeout);
        this.hideCounterTimeout = setTimeout(() => {
            this.showCounter = false;
        }, 1000);
    }

    getNextIndex(current: number, direction: Direction): number {
        if (direction === Direction.NEXT) {
            const next = this.currentIndex+1;
            if (next > this.photoUrls.length-1) {
                return 0;
            }
            return next;
        } else {
            const next = this.currentIndex-1;
            if (next < 0) {
                return this.photoUrls.length-1;
            }
            return next;
        }
    }

    @HostListener('window:keyup', ['$event'])
    keyup(event: KeyboardEvent) {
        console.log('key:', event);
        if (event.key === 'ArrowRight') {
            this.next();
            this.showArrows = false;
        } else if (event.key === 'ArrowLeft') {
            this.prev();
            this.showArrows = false;
        }
    }

    next() {
        console.log('next');
        this.nextOrPrev(Direction.NEXT);
    }

    prev() {
        console.log('prev');
        this.nextOrPrev(Direction.PREV);
    }

    toggleRybar() {
        this.showRybar = !this.showRybar;
        this.showArrows = true;
    }

    unlock() {
        const value = this.password;

        this.http.post('/service/photo/login', { password: value }).subscribe(
            (res: any) => {
                console.log('photo login success - res:', res);
                this.loadData();
            },
            (err: any) => {
                console.log('photo login error:', err);
                this.logInFinished(false);
            }
        );
    }




}
