import { Component, Input, Output, OnInit, ElementRef, ViewChild, HostListener, EventEmitter } from '@angular/core';

class Ovec {
    sleeping: boolean = false;
    standing: boolean = false;
    running: boolean = false;
    turning: boolean = false;
    runningBack: boolean = false;
    sleepingBack: boolean = false;
    missing: boolean = false;
    left: number = 0;
}

@Component({
    selector: 'app-ovece-bar',
    templateUrl: './ovece-bar.component.html',
    styleUrls: ['./ovece-bar.component.scss']
})
export class OveceBarComponent implements OnInit {

    _percentage: number = 0;

    maxCount: number = 100;
    ovece: Ovec[] = [];
    barWidth: number = 10;
    grassLeft: number = 0;
    emPx: number = 1;

    oveceMovingTimeout: any = null;

    @Output() percentageChange: EventEmitter<number> = new EventEmitter();

    constructor(private elementRef: ElementRef) {}

    @Input() set percentage(val: number) {
        this._percentage = val;
        this.updateOvece();
    }

    get percentage(): number {
        return this._percentage;
    }

    ngOnInit() {
    }

    ngAferViewInit() {
        this.updateOvece();
    }

    computeMaxCount() {
        const emsize = parseFloat(getComputedStyle(this.elementRef.nativeElement).fontSize);
        this.emPx = emsize;
        this.barWidth = this.elementRef.nativeElement.clientWidth;
        this.maxCount = Math.round(this.barWidth / (emsize * 2));
        console.log('maxCount:', this.maxCount);
        console.log('barWidth:', this.barWidth);
    }

    @HostListener('window:resize')
    updateOvece() {

        console.log('updating ovece...');

        this.computeMaxCount();

        const list: Ovec[] = [];
        const oneOvecePerc = 100 / this.maxCount;
        const onePercWidth = this.barWidth / 100 * oneOvecePerc;
        console.log('oneOvecePerc:', oneOvecePerc);
        console.log('onePercWidth:', onePercWidth);
        console.log('percentage:', this.percentage);
        let previousOvec = null;
        let oneUndecided = false;
        for (let i = 1; i <= this.maxCount; i++) {
            const perc = i*oneOvecePerc;
            const ovec = new Ovec();
            list.push(ovec);

            // set left
            ovec.left = (i - 1) * onePercWidth;

            if (perc > this.percentage) {
                ovec.missing = true;
                console.log(`iter i=${i}, missing`);
                if (previousOvec !== null && (previousOvec.sleeping || previousOvec.turning)) {
                    console.log(`iter i=${i}, setting previous standing:`, previousOvec);
                    previousOvec.sleeping = false;
                    previousOvec.turning = false;
                    previousOvec.standing = true;
                }
            } else {

                if (perc >= this.percentage / 2) {
                    if (!oneUndecided) {
                        ovec.turning = true;
                        oneUndecided = true;
                    } else {
                        ovec.sleeping = true;
                    }
                } else {

                    if (i <= 3 && this.percentage > 100 - (4-i)*oneOvecePerc) {
                        ovec.runningBack = true;
                    } else {
                        ovec.sleepingBack = true;
                    }

                }

                console.log(`iter i=${i}, sleeping`);
            }
            previousOvec = ovec;
        }
        this.ovece = list;

        console.log('ovece updated:', this.ovece);
    }

    barClicked(event: MouseEvent): void {
        console.log('event.pageX:', event.pageX);
        this.percentage = Math.ceil((event.pageX / this.barWidth) * 100);
        console.log('new percentage:', this.percentage);
        this.percentageChange.emit(this.percentage);
    }

}
