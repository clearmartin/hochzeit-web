import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-pike-overlay',
    templateUrl: './pike-overlay.component.html',
    styleUrls: ['./pike-overlay.component.scss']
})
export class PikeOverlayComponent implements OnInit {

    pikeShown: boolean = true;
    pikeStarted: boolean = false;
    pikeHide: boolean = false;
    pikeHideGood: boolean = false;

    constructor() { }

    ngOnInit(): void {
    }

    ngAfterViewInit() {
        this.showPikeFn(true);
    }

    showPikeFn(show: boolean): void {
        this.pikeShown = show;
        if (show) {
            this.pikeHide = false;
            this.pikeHideGood = false;
            this.pikeStarted = false;
            setTimeout(() => {
                this.pikeStarted = true;
            }, 10);
        } else {
            this.pikeHide = true;
            setTimeout(() => {
                this.pikeHide = false;
            }, 100);
            setTimeout(() => {
                this.pikeHide = true;
            }, 300);
            setTimeout(() => {
                this.pikeHide = false;
            }, 500);
            setTimeout(() => {
                this.pikeHideGood = true;
            }, 700);
        }
    }

}
