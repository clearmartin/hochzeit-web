import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { StateService } from './service/state.service';

import { PlaceComponent } from './components/place/place.component';
import { PikeOverlayComponent } from './components/pike-overlay/pike-overlay.component';
import { LoveLettersComponent } from './components/love-letters/love-letters.component';
import { RybarComponent } from './components/rybar/rybar.component';
import { OveceBarComponent } from './components/ovece-bar/ovece-bar.component';
import { PhotosComponent } from './components/photos/photos.component';


@NgModule({
  declarations: [
    AppComponent,
    PlaceComponent,
    PikeOverlayComponent,
    LoveLettersComponent,
    RybarComponent,
    OveceBarComponent,
    PhotosComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [
    StateService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
