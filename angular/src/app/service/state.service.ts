import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class StateService {

    public swipeLeft: EventEmitter<any> = new EventEmitter();
    public swipeRight: EventEmitter<any> = new EventEmitter();

    constructor () {}

}
