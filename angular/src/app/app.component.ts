import { Component, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { StateService } from './service/state.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
    showPlace: boolean = false;
    showLetters: boolean = false;
    showPhotos: boolean = false;

    private swipeCoord?: [number, number];
    private swipeTime?: number;

    @ViewChild('showPlaceTrigger') showPlaceTrigger?: ElementRef;

    constructor(private elementRef: ElementRef, private detectorRef: ChangeDetectorRef, private state: StateService) {}

    ngAfterViewInit() {
        if (window.location.pathname.startsWith('/dopisy')) {
            this.showLetters = true;
            this.detectorRef.detectChanges();
            return;
        }
        if (window.location.pathname.startsWith('/fotky')) {
            this.showPhotos = true;
            this.detectorRef.detectChanges();
            return;
        }
        setTimeout(() => {
            this.showPlaceTrigger?.nativeElement.classList.remove('initial');
        }, 1000);
    }

    swipe(e: TouchEvent, when: string): void {
      const coord: [number, number] = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
      const time = new Date().getTime();

      if (when === 'start') {
        this.swipeCoord = coord;
        this.swipeTime = time;
      } else if (when === 'end') {
        if (this.swipeCoord == null || this.swipeTime == null) {
          return;
        }
        const direction = [coord[0] - this.swipeCoord[0], coord[1] - this.swipeCoord[1]];
        const duration = time - this.swipeTime;

        if (duration < 1000 //
          && Math.abs(direction[0]) > 30 // Long enough
          && Math.abs(direction[0]) > Math.abs(direction[1] * 3)) { // Horizontal enough
            const swipe = direction[0] < 0 ? 'next' : 'previous';
            if (swipe === 'next') {
                this.state.swipeRight.emit();
            } else {
                this.state.swipeLeft.emit();
            }
        }
      }
    }
}
