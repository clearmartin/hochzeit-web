DELETE FROM PresentItem;
DELETE FROM PresentCategory;


INSERT INTO PresentCategory (id, title, description, ordering) VALUES (1, 'Ostatní', '(např. košík ovoce nebo zeleniny, pokud si netroufáte na vaření)', 4);
INSERT INTO PresentCategory (id, title, description, ordering) VALUES (2, 'Slané', NULL, 1);
INSERT INTO PresentCategory (id, title, description, ordering) VALUES (3, 'Sladké', NULL, 2);
INSERT INTO PresentCategory (id, title, description, ordering) VALUES (4, 'Alko, nealko', NULL, 3);

INSERT INTO PresentItem (id, title, description, categoryId, claimName, claimCode) VALUES (1, 'Bábovka', NULL, 3, 'Batman & Robin', 'wtf_lol');

-- Do úvodu popis

