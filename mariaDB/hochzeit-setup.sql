CREATE DATABASE hochzeit DEFAULT CHARACTER SET utf8;
GRANT SELECT ON hochzeit.* to 'hochzeit_read'@'localhost' IDENTIFIED BY 'hochzeit';
GRANT SELECT ON hochzeit.* to 'hochzeit_write'@'localhost' IDENTIFIED BY 'hochzeit';
GRANT INSERT ON hochzeit.* to 'hochzeit_write'@'localhost' IDENTIFIED BY 'hochzeit';
GRANT UPDATE ON hochzeit.* to 'hochzeit_write'@'localhost' IDENTIFIED BY 'hochzeit';
GRANT DELETE ON hochzeit.* to 'hochzeit_write'@'localhost' IDENTIFIED BY 'hochzeit';

--DROP DATABASE hochzeit;
--REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'hochzeit_read'@'localhost';
--REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'hochzeit_write'@'localhost';
--DROP USER 'hochzeit_read'@'localhost';
--DROP USER 'hochzeit_write'@'localhost';
