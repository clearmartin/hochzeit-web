<?php

if ($_SERVER['REQUEST_METHOD'] !== 'POST' && $_SERVER['REQUEST_METHOD'] !== 'PUT' && $_SERVER['REQUEST_METHOD'] !== 'DELETE') {
    http_response_code(400);
    echo 'The only supported methods are POST, PUT, DELETE.';
    exit();
}

$createFlag = $_SERVER['REQUEST_METHOD'] === 'POST';
$deleteFlag = $_SERVER['REQUEST_METHOD'] === 'DELETE';


if ($deleteFlag) {

    $presentId = (int) $_GET['id'];
    $claimCode = $_GET['claimCode'];

    // connect to database
    $connection = Connection::connectForReadWrite();

    $sql = 'DELETE FROM PresentItem WHERE';
    $sql = $sql . ' id=' . $presentId;
    $sql = $sql . ' AND claimCode=\'' . $connection->real_escape_string($claimCode) . '\'';

    error_log('sql: ' . $sql);
    $result = $connection->query($sql);

    if ($result && $connection->affected_rows === 1) {
        // success

        header('Content-Type: text/plain');
        echo 'deleted';

    } else {
        http_response_code(400);
        echo 'error';
    }

    exit();
}


$requestBody = file_get_contents("php://input");
error_log('requestBody: '.$requestBody);
$jsonRequest = json_decode($requestBody, true);

if (!$jsonRequest) {
    http_response_code(400);
    echo 'Request JSON is not valid.';
    exit();
}

$claimName = $jsonRequest['claimName'];
$title = $jsonRequest['title'];
$description = $jsonRequest['description'];

$query = null;

if ($createFlag) {

    if (isset($jsonRequest['claimCode']) && !empty($jsonRequest['claimCode'])) {

        $claimCode = $jsonRequest['claimCode'];

//         // connect to database
//         $connectionRead = Connection::connectForRead();
//
//         // get existing
//         $sql = 'SELECT id FROM PresentItem WHERE claimCode=' . $claimCode . ' LIMIT 1';
//         $query = mysqli_query($connectionRead, $sql);
//         $row = mysqli_fetch_array($query);


    } else {

        $claimCode = Util::randomString();

    }

    $categoryId = (int) $jsonRequest['categoryId'];

    // connect to database
    $connection = Connection::connectForReadWrite();

    $sql = 'INSERT INTO PresentItem (claimCode, categoryId, claimName, title, description) VALUES (';
    $sql = $sql . '\'' . mysqli_real_escape_string($connection, $claimCode) . '\'';
    $sql = $sql . ', ' . $categoryId;
    $sql = $sql . ', \'' . mysqli_real_escape_string($connection, $claimName) . '\'';
    $sql = $sql . ', \'' . mysqli_real_escape_string($connection, $title) . '\'';
    $sql = $sql . ', \'' . mysqli_real_escape_string($connection, $description) . '\')';

    error_log('sql: ' . $sql);
    $query = mysqli_query($connection, $sql);

} else {

    $presentId = (int) $jsonRequest['id'];

    // connect to database
    $connectionRead = Connection::connectForRead();

    // get existing
    $sql = 'SELECT * FROM PresentItem WHERE id=' . $presentId;
    $query = mysqli_query($connectionRead, $sql);
    $row = mysqli_fetch_array($query);
    $currentItem = ApiHelper::copyPresentItem($row, $connectionRead, false);

    if (!$currentItem) {
        http_response_code(400);
        echo 'Unknown id.';
        exit();
    }

    $claimCode = $jsonRequest['claimCode'];
    error_log('req claimCode: ' . $claimCode);
    error_log('cur claimCode: ' . $row['claimCode']);

    if (!isset($row['claimCode']) || $claimCode !== $row['claimCode']) {
        http_response_code(401);
        echo 'ClaimCode does not match.';
        exit();
    }

    // connect to database
    $connection = Connection::connectForReadWrite();

    $sql = 'UPDATE PresentItem SET';
    $sql = $sql . ' claimName=\'' . mysqli_real_escape_string($connection, $claimName) . '\'';
    $sql = $sql . ', title=\'' . mysqli_real_escape_string($connection, $title) . '\'';
    $sql = $sql . ', description=\'' . mysqli_real_escape_string($connection, $description) . '\'';
    $sql = $sql . ' WHERE id=' . $presentId;
    $sql = $sql . ' AND claimCode=\'' . mysqli_real_escape_string($connection, $claimCode) . '\'';

    error_log('sql: ' . $sql);
    $query = mysqli_query($connection, $sql);
}




if ($query) {
    // success

    if ($createFlag) {
        header('Content-Type: text/plain');
        echo $claimCode;
    } else {
        header('Content-Type: text/plain');
        echo 'updated successfully';
    }

} else {
    http_response_code(400);
    echo 'error';
}

?>
