<?php

if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
    http_response_code(400);
    echo 'The only supported method is GET.';
    exit();
}

$claimCode = $_GET['claimCode'];

if (!$claimCode) {
    http_response_code(400);
    echo 'Request param claimCode is not set.';
    exit();
}

// connect to database
$connection = Connection::connectForRead();

// get existing
$sql = 'SELECT * FROM PresentItem WHERE claimCode=\'' . $claimCode . '\' LIMIT 1';
$query = mysqli_query($connection, $sql);
$row = mysqli_fetch_array($query);

error_log('sql: ' . $sql);
//error_log('row: ' . $row);

if (!$row) {
    http_response_code(401);
    echo 'Unknown code.';
    exit();
}

$currentItem = ApiHelper::copyPresentItem($row, $connection, false);

$response = array();
$response['claimCode'] = $claimCode;
$response['claimName'] = $currentItem['claimName'];

header('Content-Type: application/json');
echo json_encode($response);

?>
