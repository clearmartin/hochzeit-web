<?php

// connect to database
$connection = Connection::connectForRead();

$claimCode = $_GET['claimCode'];

$sql = 'SELECT id, title, description, claimName, claimCode, claimCreated, claimModified, categoryId FROM PresentItem ORDER BY id DESC;';
error_log($sql);
$result = $connection->query($sql);

$items = array();
while ($row = $result->fetch_array()) {
    $item = ApiHelper::copyPresentItem($row, $connection);
    $item['editable'] = $claimCode === $row['claimCode'];
    array_push($items, $item);
}

$sql = 'SELECT id, title, description FROM PresentCategory ORDER BY ordering;';
error_log($sql);
$result = $connection->query($sql);

$categories = array();
while ($row = $result->fetch_array()) {
    $category = array();
    $category['id'] = (int) $row['id'];
    $category['title'] = $row['title'];
    $category['description'] = $row['description'];
    $categoryItems = array();


    foreach($items as $key => $item) {
        if ($category['id'] === $item['categoryId']) {
            array_push($categoryItems, $item);
            unset($items[$key]);
        }
    }

    $category['items'] = $categoryItems;

    array_push($categories, $category);
}

header('Content-Type: application/json; charset=UTF-8');
echo json_encode($categories);

?>
