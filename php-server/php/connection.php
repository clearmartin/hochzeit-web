<?php

class Connection {
    private static $DB_SERVER_NAME = "127.0.0.1";
    private static $DB_NAME = "hochzeit";
    private static $DB_READ_USER = "hochzeit_read";
    private static $DB_WRITE_USER = "hochzeit_write";
    private static $DB_PASSWORD = "hochzeit";

    public static function connectForRead() {
      $connection = mysqli_connect(self::$DB_SERVER_NAME, self::$DB_READ_USER, self::$DB_PASSWORD, self::$DB_NAME) or die('Unable to connect');
      return $connection;
    }

    public static function connectForReadWrite() {
      $connection = mysqli_connect(self::$DB_SERVER_NAME, self::$DB_WRITE_USER, self::$DB_PASSWORD, self::$DB_NAME) or die('Unable to connect');
      return $connection;
    }

}

?>
