<?php

class Util {

    static function randomString(int $length = 50, string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'): string {
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces []= $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }

}

class ApiHelper {

    static function copyPresentItem($db_row, $connection) {
        $copy = array();
        $copy['id'] = (int) $db_row['id'];
        $copy['title'] = $db_row['title'];
        $copy['description'] = $db_row['description'];
        $copy['claimName'] = $db_row['claimName'];
        $copy['claimCreated'] = $db_row['claimCreated'];
        $copy['claimModified'] = $db_row['claimModified'];
        $copy['categoryId'] = (int) $db_row['categoryId'];
        return $copy;
    }

}

?>
