<?php

// headers
// header('Access-Control-Allow-Origin: *');
// header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
// header("Access-Control-Allow-Headers: Origin, Content-Type");

// Handling the Preflight
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') { 
    exit;
}

$KNOWN_API_PATHS = ['/presents', '/present', '/login'];

if (!isset($_GET['path'])) {
    http_response_code(400);
    echo 'You need to set a path parameter.';
    exit();
}

$api_path = $_GET['path'];

if (!in_array($api_path, $KNOWN_API_PATHS)) {
    http_response_code(404);
    echo 'Unknown path.';
    exit();
}

// includes
include './php/connection.php';
include './php/util.php';

include "./php/api$api_path.php";

?>
