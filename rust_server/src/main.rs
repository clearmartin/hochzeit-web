use actix_session::config::CookieContentSecurity;
use actix_session::SessionMiddleware;
use actix_session::storage::CookieSessionStore;
use actix_web::{get, web, App, HttpResponse, HttpServer, Responder, cookie::SameSite, middleware::Logger, cookie::Key};
//use actix_service::Service;
//use futures::future::FutureExt;
use std::env;
use env_logger::Env;
use rand::Rng;

mod db;
mod app;
mod route;


#[get("/")]
async fn welcome() -> impl Responder {
    HttpResponse::Ok().body("Hochzeit Server lebt für Ihren Genüss")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {

    let db_pool = db::create_pool(5).await.unwrap();

    let host = env::var("BIND_HOST").expect("BIND_HOST is not set in .env file");
    let port = env::var("BIND_PORT").expect("BIND_PORT is not set in .env file");

    let photo_password = env::var("PHOTO_PASSWORD").expect("PHOTO_PASSWORD is not set in .env file");
    let photo_path_base = env::var("PHOTO_PATH_BASE").expect("PHOTO_PATH_BASE is not set in .env file");
    let photo_path_full = env::var("PHOTO_PATH_FULL").expect("PHOTO_PATH_FULL is not set in .env file");
    let static_files_path = env::var("STATIC_FILES_PATH").expect("STATIC_FILES_PATH is not set in .env file");

    env_logger::Builder::from_env(Env::default().default_filter_or("debug")).init();

    let private_key = rand::thread_rng().gen::<[u8; 32]>();

    let server = HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            //.wrap(DefaultHeaders::new().header("Cache-Control", "no-store"))
            .wrap(
                SessionMiddleware::builder(CookieSessionStore::default(), Key::derive_from( &private_key))
                    .cookie_content_security(CookieContentSecurity::Private)
                    .cookie_same_site(SameSite::Strict)
                    .build() // <- create cookie based session middleware
            )
            .app_data(web::Data::new(app::AppData {
                pool: db_pool.clone(),
                photo_password: photo_password.clone(),
                photo_path_base: photo_path_base.clone(),
                photo_path_full: photo_path_full.clone(),
                static_files_path: static_files_path.clone(),
            })) // pass database pool to application so we can access it inside handlers
            .service(welcome)
            .configure(route::present_init)
            .configure(route::photo_init)
    });
    server.bind(format!("{}:{}", host, port))?
        .run()
        .await
}
