use crate::db::DbPool;

pub struct AppData {
    pub pool: DbPool,
    pub photo_password: String,
    pub photo_path_base: String,
    pub photo_path_full: String,
    pub static_files_path: String,
}