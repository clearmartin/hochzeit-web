mod present;
mod photo;

pub use present::init as present_init;
pub use photo::init as photo_init;
