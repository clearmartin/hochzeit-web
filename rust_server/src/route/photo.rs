use std::path::PathBuf;

use actix_files::NamedFile;
use actix_session::Session;
use actix_web::{Either, get, HttpRequest, HttpResponse, post, Responder, web};
use actix_web::http::StatusCode;
use actix_web::middleware::DefaultHeaders;
use serde::Deserialize;

use crate::app::AppData;

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
enum ManifestMode {
    Base, Full
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct ManifestQuery {
    mode: ManifestMode,
}

#[derive(Deserialize)]
struct LoginData {
    password: String,
}

fn is_logged_in(session: Session) -> bool {
    match session.get::<bool>("loggedIn") {
        Ok(res) => {
            log::debug!("is_logged_in res is none: {}", res.is_none());
            match res {
                Some(true) => true,
                _ => false,
            }
        },
        _ => false,
    }
}

#[post("/login")]
async fn login(session: Session, data: web::Data<AppData>, query: web::Json<LoginData>) -> impl Responder {

    log::debug!("login");

    let matched = query.password == data.get_ref().photo_password;
    match session.insert("loggedIn", matched) {
        Ok(_) => {
            log::debug!("loggedIn: {}", session.get::<bool>("loggedIn").unwrap().unwrap());

            if matched {
                HttpResponse::Ok()
            } else {
                HttpResponse::Unauthorized()
            }
        }
        Err(_) => HttpResponse::Unauthorized()
    }
}

#[get("/static/{filename:.*}")]
async fn get_photo_file(session: Session, data: web::Data<AppData>, req: HttpRequest, query: web::Query<ManifestQuery>) -> Either<impl Responder, HttpResponse> {

    log::debug!("get_photo_file");

    if is_logged_in(session) {
        let mut path = match &query.mode {
            ManifestMode::Base => PathBuf::from(&data.get_ref().photo_path_base),
            ManifestMode::Full => PathBuf::from(&data.get_ref().photo_path_full),
        };

        let filename: PathBuf = req.match_info().query("filename").parse().unwrap();
        path.push(filename.to_str().unwrap());

        log::debug!("requested file: {}", filename.to_str().unwrap());

        match NamedFile::open(path) {
            Ok(file) => {

                let res = file
                    .use_last_modified(true)
                    .disable_content_disposition();

                if filename.to_str().unwrap() == "manifest.json" {
                    Either::Left(res.customize().with_status(StatusCode::OK))
                } else {
                    Either::Left(res.customize().insert_header(("Cache-Control", "private")))
                }
            }
            _ => Either::Right(HttpResponse::NotFound().finish())
        }

    } else {
        Either::Right(HttpResponse::Unauthorized().finish())
    }
}

pub fn init(cfg: &mut web::ServiceConfig) {
    cfg.service(web::scope("/photo")
        .wrap(DefaultHeaders::new().add(("Cache-Control", "no-store")))
        .service(login)
        .service(get_photo_file)
    );
}