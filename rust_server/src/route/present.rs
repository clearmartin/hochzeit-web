use crate::db::{PresentCategory, PresentItem, CreateError};
use crate::app::AppData;
use actix_web::{get, post, put, delete, web, HttpResponse, Responder};
use serde::{Deserialize, Serialize};
use chrono::{DateTime, Utc};
use actix_web::middleware::DefaultHeaders;


#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct Category {
    pub id: i32,
    pub title: String,
    pub description: Option<String>,
    pub items: Vec<Present>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct Present {
    pub id: i32,
    pub title: String,
    pub description: Option<String>,
    pub claim_name: String,
    pub claim_created: DateTime<Utc>,
    pub claim_modified: DateTime<Utc>,
    pub editable: bool,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct PresentsQuery {
    claim_code: String,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct DeletePresentQuery {
    id: i32,
    claim_code: String,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct UpdatePresentQuery {
    claim_code: String,
    claim_name: String,
    category_id: i32,
    title: String,
    description: String,
    id: i32,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct CreatePresentQuery {
    claim_code: Option<String>,
    claim_name: String,
    category_id: i32,
    title: String,
    description: String,
}

#[get("/login")]
async fn get_login(data: web::Data<AppData>, query: web::Query<PresentsQuery>) -> impl Responder {
    let result = PresentItem::find_first(&data.get_ref().pool, &query.claim_code).await;
    match result {
        Ok(login_result) => match login_result {
            Some(login) => HttpResponse::Ok().json(login),
            None => HttpResponse::NotFound().body("code_not_found"),
        }
        _ => HttpResponse::BadRequest().body("db_error"),
    }
}

#[get("/list")]
async fn get_presents(data: web::Data<AppData>, query: web::Query<PresentsQuery>) -> impl Responder {
    let categories = PresentCategory::find_all(&data.get_ref().pool);
    let presents = PresentItem::find_all(&data.get_ref().pool).await.unwrap();
    let mut category_results = Vec::new();
    for category in categories.await.unwrap() {
        let mut cat = Category {
            id: category.id,
            title: category.title,
            description: category.description,
            items: Vec::new()
        };
        for present in presents.to_vec() {
            if present.category_id == category.id {
                cat.items.push(Present {
                    id: present.id,
                    title: present.title,
                    description: present.description,
                    claim_name: present.claim_name,
                    claim_created: present.claim_created,
                    claim_modified: present.claim_modified,
                    editable: present.claim_code == query.claim_code,
                });
            }
        }
        category_results.push(cat);
    }
    return HttpResponse::Ok().json(category_results);
}

#[delete("")]
async fn delete_present(data: web::Data<AppData>, query: web::Query<DeletePresentQuery>) -> impl Responder {
    let result = PresentItem::delete_one(&data.get_ref().pool, &query.id, &query.claim_code).await;
    match result {
        Ok(true) => HttpResponse::Ok().body("success"),
        Ok(false) => HttpResponse::NotFound().body("not_exist"),
        _ => HttpResponse::BadRequest().body("db_error"),
    }
}

#[put("")]
async fn update_present(data: web::Data<AppData>, query: web::Json<UpdatePresentQuery>) -> impl Responder {
    let result = PresentItem::update_one(&data.get_ref().pool, &query.claim_code, &query.claim_name, &query.category_id, &query.title, &query.description, &query.id).await;
    match result {
        Ok(true) => HttpResponse::Ok().body("success"),
        Ok(false) => HttpResponse::NotFound().body("not_exist"),
        _ => HttpResponse::BadRequest().body("db_error"),
    }
}

#[post("")]
async fn create_present(data: web::Data<AppData>, query: web::Json<CreatePresentQuery>) -> impl Responder {
    let result = PresentItem::create_one(&data.get_ref().pool, &query.claim_code, &query.claim_name, &query.category_id, &query.title, &query.description).await;
    match result {
        Ok(claim_code) => HttpResponse::Ok().body(claim_code),
        Err(CreateError::Unauthorized) => HttpResponse::Unauthorized().body("unauthorized"),
        _ => HttpResponse::BadRequest().body("db_error"),
    }
}


// function that will be called on new Application to configure routes for this module
pub fn init(cfg: &mut web::ServiceConfig) {
    cfg.service(web::scope("/present")
        .wrap(DefaultHeaders::new().add(("Cache-Control", "no-store")))
        .service(get_presents)
        .service(get_login)
        // .service(delete_present)
        // .service(update_present)
        // .service(create_present)
    );
}