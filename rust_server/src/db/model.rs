use serde::{Deserialize, Serialize};
use sqlx::{FromRow};
use chrono::{DateTime, Utc, TimeZone};
use rand::{distributions::Alphanumeric, Rng};

use super::connection;
use connection::DbPool;

#[derive(Serialize, FromRow)]
#[serde(rename_all = "camelCase")]
pub struct PresentCategory {
    pub id: i32,
    pub title: String,
    pub description: Option<String>,
    pub ordering: i32,
}

#[derive(Serialize, Deserialize, FromRow, Clone)]
#[serde(rename_all = "camelCase")]
pub struct PresentItem {
    pub id: i32,
    pub category_id: i32,
    pub title: String,
    pub description: Option<String>,
    pub claim_name: String,
    pub claim_created: DateTime<Utc>,
    pub claim_modified: DateTime<Utc>,
    pub claim_code: String,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Login {
    pub claim_name: String,
    pub claim_code: String,
}

pub type CreateResult = Result<String, CreateError>;
pub enum CreateError {
    Unauthorized,
    DbError,
}

impl PresentCategory {
    pub async fn find_all(db_pool: &DbPool) -> std::io::Result<Vec<PresentCategory>> {
        let recs: Vec<PresentCategory> = sqlx::query_as!(PresentCategory,
            r#"
                SELECT id, title, description, ordering
                FROM PresentCategory
                ORDER BY ordering
            "#
        ).fetch_all(db_pool).await.unwrap();
        Ok(recs)
    }
}

impl PresentItem {
    pub async fn find_all(db_pool: &DbPool) -> std::io::Result<Vec<PresentItem>> {
        let mut items = vec![];
        let rows = sqlx::query!(
            r#"
                SELECT id, categoryId, title, description, claimName, claimCreated, claimModified, claimCode
                FROM PresentItem
                ORDER BY id
            "#
        ).fetch_all(db_pool).await.unwrap();

        for row in rows {
            items.push(PresentItem {
                id: row.id,
                category_id: row.categoryId,
                title: row.title,
                description: row.description,
                claim_name: row.claimName,
                claim_created: chrono_tz::Europe::Prague.from_local_datetime(&row.claimCreated).unwrap().with_timezone(&Utc),
                claim_modified: chrono_tz::Europe::Prague.from_local_datetime(&row.claimModified).unwrap().with_timezone(&Utc),
                claim_code: row.claimCode,
            });
        }

        Ok(items)
    }

    pub async fn find_first(db_pool: &DbPool, claim_code: &String) -> std::io::Result<Option<Login>> {
        let row: Option<Login> = sqlx::query_as!(Login,
            r#"
                SELECT claimName as claim_name, claimCode as claim_code
                FROM PresentItem
                WHERE claimCode = ?
                LIMIT 1
            "#,
            claim_code
        ).fetch_optional(db_pool).await.unwrap();
        Ok(row)
    }

    pub async fn delete_one(db_pool: &DbPool, id: &i32, claim_code: &String) -> std::io::Result<bool> {
        let rows_affected = sqlx::query!(
            r#"
                DELETE FROM PresentItem
                WHERE id = ? AND claimCode = ?
            "#,
            id, claim_code
        ).execute(db_pool).await.unwrap().rows_affected();
        Ok(rows_affected > 0)
    }

    pub async fn update_one<'a>(db_pool: &DbPool, claim_code: &String, claim_name: &String, category_id: &i32, title: &String, description: &String, id: &i32) -> Result<bool, &'a str> {
        let result = sqlx::query!(
            r#"
                UPDATE PresentItem
                SET title = ?, description = ?, claimName = ?
                WHERE id = ? AND categoryId = ? AND claimCode = ?
            "#,
            title, description, claim_name, id, category_id, claim_code
        ).execute(db_pool).await;
        match result {
            Ok(res) => Ok(res.rows_affected() > 0),
            Err(_) => Err(&"db_error"),
        }
    }

    pub async fn create_one(db_pool: &DbPool, claim_code: &Option<String>, claim_name: &String, category_id: &i32, title: &String, description: &String) -> CreateResult {
        match claim_code {
            Some(claim_code) => {
                match PresentItem::find_first(db_pool, claim_code).await.unwrap() {
                    Some(login) => {
                        let result = sqlx::query!(
                            r#"
                                INSERT PresentItem (categoryId, title, description, claimName, claimCode)
                                VALUES (?, ?, ?, ?, ?)
                            "#,
                            category_id, title, description, claim_name, login.claim_code
                        ).execute(db_pool).await;
                        match result {
                            Ok(_) => Ok(login.claim_code),
                            _ => Err(CreateError::DbError)
                        }
                    }
                    None => Err(CreateError::Unauthorized)
                }
            }
            None => {
                let claim_code_generated: String = rand::thread_rng().sample_iter(&Alphanumeric).take(50).map(char::from).collect();
                let result = sqlx::query!(
                    r#"
                        INSERT PresentItem (categoryId, title, description, claimName, claimCode)
                        VALUES (?, ?, ?, ?, ?)
                    "#,
                    category_id, title, description, claim_name, claim_code_generated
                ).execute(db_pool).await;
                match result {
                    Ok(_) => Ok(claim_code_generated),
                    _ => Err(CreateError::DbError)
                }
            }
        }
    }
}
