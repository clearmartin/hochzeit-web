#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"

ps_lines=`ps -ef | grep "target/release/hochzeit-web-server" | grep -v grep`
echo $ps_lines
if [ "${ps_lines}" == "" ]; then
    echo "Server is not running."
    exit 0
fi

pid=`echo $ps_lines | awk '{ print $2 }'`
echo "Stopping server PID=${pid}"
kill -s TERM ${pid}
echo "Server stopped"
