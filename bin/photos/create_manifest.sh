#!/bin/bash

{
  echo "["
  for f in `find . -type f | sort | rg "(.webp|.avif)$"`; do
    echo "  \"$f\","
  done
  echo "]"
} | sd ",\n]" "\n]" > manifest.json
