#!/bin/bash

avif_enabled=false

for f in `find source/ -type f | sort | grep -i ".jpg$"`; do
  f_base=`echo "$f" | cut -d'.' -f1 | sed -e 's/source\///'`
  f_target="converted/${f_base}.webp"
  mkdir -p "$(dirname "${f_target}")"

  if [ "$avif_enabled" = true ]; then
    f_target="converted/${f_base}.webp"
    f_temp="converted/${f_base}.avif"
    echo "Converting $f to ${f_target}..."
    convert "$f" -scale 35% -auto-orient "${f_temp}"
    time cavif "${f_temp}" -o "${f_target}"
    rm ${f_temp}
    continue
  fi

  echo "Converting $f to ${f_target}..."
  time convert "$f" -scale 35% -auto-orient "${f_target}"
done

echo "all done"
