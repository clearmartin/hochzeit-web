#!/bin/bash

curl -X GET "http://localhost:8080/login?claim_code=wtf_lol";

curl -X GET "http://localhost:8080/presents?claim_code=wtf_lol";

curl -X POST "http://localhost:8080/present" -H "Content-Type: application/json" --data '{"category_id": 3, "title": "Nákyp", "description": "", "claim_name": "Batman", "claim_code": "wtf_lol"}';

curl -X PUT "http://localhost:8080/present" -H "Content-Type: application/json" --data '{"category_id": 3, "title": "Nákyp2+ěščřžýáíéůú_", "description": "", "claim_name": "Batman", "claim_code": "wtf_lol", "id": 28}';

curl -X DELETE "http://localhost:8080/present?claim_code=wtf_lol&id=25";
